# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.0.0](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v2.0.1...v3.0.0) (2019-10-24)


### ⚠ BREAKING CHANGES

* Assigning values now happens inside constructor

### Bug Fixes

* **app:** change links to stuff ([eee8ac3](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/eee8ac38117fdf3ab6764cd7ddf1b4c8f371bb27))


### Code Refactoring

* modify variables initialisation ([7421d1d](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/7421d1df84b8f82ca50eb011a65482f6443cf356))

### [2.0.1](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v2.0.0...v2.0.1) (2019-10-21)


### Feature Improvements

* **landing:** change !!! to !! ([9c6f460](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/9c6f4609fe973cb7846befbeedcd38d93f945354))

## [2.0.0](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v1.0.0...v2.0.0) (2019-10-17)


### ⚠ BREAKING CHANGES

* **app:** add comment to app.component.ts

### Feature Improvements

* **app:** [CPE-2012] add comment ([9bbbcd0](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/9bbbcd0b70c250914b2507f21868884270155d21))

## [1.0.0](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v0.2.0...v1.0.0) (2019-10-17)


### ⚠ BREAKING CHANGES

* Deliberately bump major ver

### Styling

* add empty <div> ([e3cf4fa](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/e3cf4fabb274782687237f37587d128bed6145d7))

## [0.2.0](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v0.1.0...v0.2.0) (2019-10-17)


### ⚠ BREAKING CHANGES

* Remove <h2> and change improvement to Feature Improvements in CHANGELOG.md

CPE-2010

### Others

* fix typo on.versionrc and delete <h2> ([ae930ab](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/ae930abdb0d73646bfeb10b653cb74ee752640d3))

## [0.1.0](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v0.0.2...v0.1.0) (2019-10-17)


### ⚠ BREAKING CHANGES

* Description of breaking changes

### improvement

* add h2 tag ([022fa84](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/022fa848442e33a5f5c10302e5a07f21f144b0e8))

### [0.0.2](https://bitbucket.org/christian_sunardi/angular-standard-version/compare/v0.0.1...v0.0.2) (2019-10-17)


### Features

* [CPE-2010] Add !! to app.component.html ([50b82d0](https://bitbucket.org/christian_sunardi/angular-standard-version/commit/50b82d0ae3ea14465e70888841e09e233973047c))

### 0.0.1 (2019-10-17)


### Features

* implement standard-version and commitizen 49a8b28


### Build System

* fix .versionrc syntax errors and npm i -D standard-version 3e0027f
